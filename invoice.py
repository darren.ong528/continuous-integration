'''
INVOICE
Simple command line test program.
Select an item, quantity from which invoice is created.
Requires a stock.txt file, which contains items and prices.
'''
# constants
VAT_RATE = 0.22
DISCOUNT = 0.1
DISCOUNT_THRESHOLD = 20

def load_stock(stock_file):
    '''
    Load data from stock file into data list.
    Each line of the file holds name and price
    This information is loaded into a list which holds a list of id, name and price for each item.
    data = [ [id, name, price], ... ]
    '''
    data = []
    with open(stock_file, encoding="UTF-8") as stock_file:
        for line in stock_file:
            line_split = line.split(',')
            data.append([line_split[0].strip(), float(line_split[1].strip()) ])
    return data

def item_list(stock_data):
    '''
    This prints out the list of items, prefixed with each item id
    The item id can be used to select the required item
    '''
    formatted_item_list = []
    for idx, item in enumerate(stock_data):
        formatted_item_list.append( f"{idx+1} : {item[0]}\n" )
    return formatted_item_list


def get_item_name(stock_data, selected_item):
    '''
    Returns the item name from the stock data structure for the selected line.
    '''
    return stock_data[selected_item][0]


def get_price(stock_data, selected_item):
    '''
    Returns the price for the given item from the stock data structure. 
    '''
    price = stock_data[selected_item][1]
    return price

def calculate_vat(amount):
    '''
    Calculates the VAT to be paid on an amount.
    '''
    return amount * VAT_RATE

def discount(amount):
    '''
    Calculates the amount of discount to apply.
    Amound over discount threshold has the discount amount applied
    '''
    if amount > DISCOUNT_THRESHOLD:
        return round( amount * DISCOUNT,2)
    return 0


def create_invoice_data(stock_data, selected_item, quantity):
    '''
    Produce all the calculated values using the above functions.
    Returns the multiple calculated values in a list, rounded to 2dp
    '''
    price = get_price(stock_data, selected_item)
    total_before_discount = price*quantity
    dis = discount(total_before_discount)
    total_gross = total_before_discount - dis
    vat = calculate_vat(total_gross)
    total_net = total_gross + vat
    return [round(total_before_discount,2),
           round(dis,2),
           round(vat,2),
           round(total_net,2)]


def generate_invoice(stock_data, selected_item, quantity):
    '''
    Print out the invoice on the console given stock, selected item and quantity
    '''
    total_before_discount, discnt, vat, total_net =  \
        create_invoice_data(stock_data, selected_item, quantity)
    invoice = []
    invoice.append("             INVOICE\n")
    invoice.append("=================================\n")
    invoice.append(f"Item:\t\t|  {get_item_name(stock_data, selected_item)}\n")
    invoice.append(f"Item Price:\t|  {get_price(stock_data, selected_item)}\n")
    invoice.append(f"Quantity:\t|  {quantity}\n")
    invoice.append(f"Total:\t\t|  {total_before_discount}\n")
    invoice.append(f"Discount:\t|  {discnt}\n")
    invoice.append(f"VAT:\t\t|  {vat}\n")
    invoice.append(f"Net Total:\t|  {total_net}\n")
    return invoice



if __name__ =="__main__":
    stock = load_stock("stock.txt")
    print( ''.join(item_list(stock)) )
    selectedItem = int(input("Enter Item:")) - 1
    selected_quantity = int(input("Enter Amount:"))
    print()
    print(''.join(generate_invoice(stock, selectedItem, selected_quantity)))
