
'''
TESTS FOR INVOICE.PY
'''
import pytest
from invoice import load_stock, get_price, get_item_name,  \
    create_invoice_data, item_list, calculate_vat, \
          discount, generate_invoice

def test_load_stock_check_size():
    '''
    Test the load_stock function returns the correct number of items
    '''
    data = load_stock("stock.txt")
    assert len(data) == 7


#  add tests for each function in invoice.py below...

@pytest.fixture
def stock():
    '''
    Load a stocks from a test stock file
    '''
    return load_stock("./tests/stock.txt")


# fixtures
@pytest.fixture
def stock_mock():
    '''
    Mock the return of items from a file for testing.
    '''
    return [["pencil",0.15],['printer paper',1.5],['notepad',1.0],['folder',1.4]]





@pytest.mark.parametrize("input,output",[(0, ["pencil", 0.15]), 
                        (1,['printer paper',1.5]),
                        (5,['notebook',1.0]),
                        (6,['folder',1.4])])
def test_load_stock_file(stock, input, output):
    '''
    Check the load_stock function returns the correct stock data.
    The file is loaded from the tests directory
    check first two and last two stock content is correct.
    '''
    #stock = load_stock("./tests/stock.txt")
    assert stock[input] == output


#  add tests for each function in invoice.py below...
@pytest.mark.parametrize("input,output",[(0, '1 : pencil'),(1,'1 : printer paper'),(2,'2 : notepad'),(3,'3 : folder')])
def test_item_list_valid_values(stock_mock, input, output):
    '''
    Test the itemList function to test it returns the correct values for each stock item.
    The stock_mock fixture returns a list of mocked stock items and parametrize compares
    each index number entered to the correct return string for the stock.  
    '''
    out = item_list(stock_mock)
    out[input] = output

@pytest.mark.parametrize("input,output",[(0, 'pencil'),(1,'printer paper'),(2,'notepad'),(3,'folder')])
def test_get_item_name_valid_values(stock_mock, input, output):
    '''
    Test the getItemName function, which returns the item name for the given index number.
    Parmetrize compares four values of input to output
    '''
    assert get_item_name(stock_mock,input) == output

@pytest.mark.parametrize("input,output",[(0, 0.15),(1,1.5),(2,1.0),(3,1.4)])
def test_get_price(stock_mock, input, output):
    '''
    Test get_price.  Pass in the index number for a item and check the returned price is correct.
    Paramtrize checks four prices are returned correctly.
    '''
    assert get_price(stock_mock, input) == output

def test_calculate_vat():
    '''
    Test that calculate_vat function is calculating the correct amount.
    '''
    assert calculate_vat(100) == 22

def test_discount_below_threshold():
    '''
    Test discount at two points where discount should not be applied  
    Ensure the highest amount at which discount is not applied returns 0.
    '''
    assert discount(0) == 0
    assert discount(20) == 0.0

def test_discount_above_threshold():
    '''
    Test discount at two point where discount should be applied 
    Ensure the lowest amount discount should be applied is 
    '''
    assert discount(21) == 2.1
    assert discount(100) == 10


def test_create_invoice_data_without_discount(stock_mock):
    '''
    Test create_invoice_data function to ensure all calculations are correctly applied.
    The mocked list of stocks is used and a specific case is tested.
    The first test checks the case where no discount should be applied.
    '''
    total_before_discount, discnt, vat, total_net = create_invoice_data(stock_mock, 0, 10)
    assert total_before_discount == 1.50
    assert discnt == 0
    assert vat == 0.33
    assert total_net == 1.83

def test_create_invoice_data_with_discount(stock_mock):
    '''
    Test create_invoice_data function to ensure all calculations are correctly used.
    The mocked list of stocks is used and a specific case is tested.
    This test checks the case where discount should be applied.
    '''
    total_before_discount, discnt, vat, total_net = create_invoice_data(stock_mock, 2, 21)
    assert total_before_discount == 21.00
    assert discnt == 2.1
    assert vat == 4.16
    assert total_net == 23.06

@pytest.mark.parametrize("input,output", 
                [([0,10], ["pencil", 0.15, 1.5, 0, 0.33, 1.83]), 
                 ([1,10],['printer paper',1.5, 15.0, 0, 3.3, 18.3]), 
                 ([6,20],['folder',1.4, 28.0, 2.8, 5.54,30.74])] )
def test_generate_invoice_1(stock, input, output):
    '''
    test generate_invoice to ensure that the function prints out the invoice data correctly.
    Test: Input: selected item, quantity.
    Test: Output: item name, total, discount, vat, net total
    Each of the lines of the formatted output are checked.
    Values are calculated from quantity and price for the given scenario.
    '''
    out = generate_invoice(stock, input[0], input[1])
    assert out[0] == "             INVOICE\n"
    assert out[1] ==  "=================================\n"  
    assert out[2] == f"Item:\t\t|  {output[0]}\n"
    assert out[3] == f"Item Price:\t|  {output[1]}\n"
    assert out[4] == f"Quantity:\t|  {input[1]}\n"
    assert out[5] == f"Total:\t\t|  {output[2]}\n"
    assert out[6] == f"Discount:\t|  {output[3]}\n"
    assert out[7] == f"VAT:\t\t|  {output[4]}\n"
    assert out[8] == f"Net Total:\t|  {output[5]}\n"
